//============================================================================
// Name        : measure_circles_area.cpp
// Author      : Guido Polles
// Version     :
// Copyright   : 
// Description : Changed description
//============================================================================

#include <iostream>
#include <fstream>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <list>
#include <cmath>

//added comment
using namespace std;

#define dist(a,b) (sqrt( (a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])))
#define distSQ(a,b) (( (a[0]-b[0])*(a[0]-b[0])+(a[1]-b[1])*(a[1]-b[1])))

#define NMaxPoints (20000)
#define BIG (10000000)

void get_vertices(float v[4][2] , float cell_center[2], float cell_size){
  v[0][0] = cell_center[0] - cell_size;
  v[0][1] = cell_center[1] - cell_size;

  v[1][0] = cell_center[0] + cell_size;
  v[1][1] = cell_center[1] - cell_size;

  v[2][0] = cell_center[0] - cell_size;
  v[2][1] = cell_center[1] + cell_size;

  v[3][0] = cell_center[0] + cell_size;
  v[3][1] = cell_center[1] + cell_size;
}

int n_partial_squares = 0;
const float sqrt2 = sqrt(2);
float check_cell( float v[4][2], // vertexes
    float grid_c[2], // cell center
    float size, // half of the cell edge
    float min_size, // minimum size of the cell
    list<float*>& centers, // centers of circles
    float radius // radius of circles
)
{
  if (size < min_size) {
    ++n_partial_squares;
    return 0;
  }
  float radiusSQ = radius*radius;

  list<float* > new_centers;
  for (float* c : centers) {

    // check if the circle lie out of the cell
    if ( fabs( grid_c[0] - c[0] ) > size + radius
        || fabs( grid_c[1] - c[1] ) > size + radius
        || dist(grid_c,c) > size*sqrt2 + radius )
      continue;

    // check if the cell is completely covered
    // i.e. all vertexes lie under a unique circle
    bool covered = true;
    for (int i = 0; i < 4; ++i) {
      covered = covered && ( distSQ(v[i],c) < radiusSQ );
    }
    if(covered) return 4*size*size; // the cell edge is 2*size, hence the 4

    // if we get here:
    // the circle c overlaps the cell without covering it completely
    // we keep the circle for next iteration
    new_centers.push_back(c);
  }

  // if there are no circles in range, the cell is empty
  if(new_centers.empty()) return 0;

  // if we got here it means that we have a partial cell.
  // we subdivide it in 4 and rerun the check for every "child cell"
  float new_grid_c[2];
  float new_v[4][2];

  float area = 0;

  // NORTH-EAST
  new_grid_c[0] = grid_c[0] + size/2;
  new_grid_c[1] = grid_c[1] + size/2;

  get_vertices(new_v,new_grid_c, size/2);
  area += check_cell(new_v,new_grid_c,size/2,min_size,new_centers,radius);

  // NORTH-WEST
  new_grid_c[0] = grid_c[0] - size/2;
  new_grid_c[1] = grid_c[1] + size/2;

  get_vertices(new_v,new_grid_c, size/2);

  area += check_cell(new_v,new_grid_c,size/2,min_size,new_centers,radius);

  // SOUTH-EAST
  new_grid_c[0] = grid_c[0] + size/2;
  new_grid_c[1] = grid_c[1] - size/2;

  get_vertices(new_v,new_grid_c, size/2);
  area += check_cell(new_v,new_grid_c,size/2,min_size,new_centers,radius);

  // SOUTH-WEST
  new_grid_c[0] = grid_c[0] - size/2;
  new_grid_c[1] = grid_c[1] - size/2;

  get_vertices(new_v,new_grid_c, size/2);
  area += check_cell(new_v,new_grid_c,size/2,min_size,new_centers,radius);

  return area;

}


int main(int argc, char* argv[]) {

  float coord[NMaxPoints][2];

  float min_edge_size = 0.001;

  int len=0;
  FILE* fp = fopen(argv[1],"r");
  list<float*> circles;
  float avedist = 0;
  float max_x=-BIG, min_x=BIG, max_y = -BIG, min_y = BIG;
  while (fscanf(fp,"%f %f",&coord[len][0],&coord[len][1])!=EOF){
    max_x = max(max_x,coord[len][0]);
    min_x = min(min_x,coord[len][0]);
    max_y = max(max_y,coord[len][1]);
    min_y = min(min_y,coord[len][1]);
    circles.push_back(coord[len]);
    if(len) avedist += dist(coord[len],coord[len-1]);
    len++;
  }
  fclose(fp);
  printf("average distance: %f \n", avedist/len );


  for(float radius = 1.0; radius <= 3.0; radius += 1.0){

    max_x += radius;
    max_y += radius;
    min_x -= radius;
    min_y -= radius;
    float size_x = max_x - min_x;
    float size_y = max_y - min_y;

    float area = 0;
    if (size_x >= size_y){
      float cell_size = size_y/2;
      for (int i = 0; i < size_x/size_y ; ++i) {
        float cell_center[2];
        float v[4][2];
        cell_center[0] = min_x + cell_size + size_y*i;
        cell_center[1] = min_y + cell_size;

        get_vertices(v,cell_center, cell_size);

        area += check_cell(v, cell_center, cell_size, min_edge_size, circles, radius);
      }
    }

    if(size_y > size_x){
      float cell_size = size_x/2;
      for (int i = 0; i < size_y/size_x ; ++i) {
        float cell_center[2];
        float v[4][2];
        cell_center[0] = min_x + cell_size ;
        cell_center[1] = min_y + cell_size + size_x*i;

        get_vertices(v,cell_center, cell_size);

        area += check_cell(v, cell_center, cell_size, min_edge_size, circles, radius);
      }
    }

    printf("radius: %f  area_min: %f  area_max: %f \n ",radius, area, area+4*min_edge_size*min_edge_size*n_partial_squares);
    n_partial_squares = 0;
  }

  return 0;
}
